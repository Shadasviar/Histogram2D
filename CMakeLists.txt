cmake_minimum_required(VERSION 3.25)

project(Histogram2D
  VERSION 1.1
  LANGUAGES CXX
)

set(CMAKE_AUTOUIC_SEARCH_PATHS "UI")

find_package(Qt6 REQUIRED COMPONENTS
  Widgets
  Charts
  DataVisualization
)

qt_standard_project_setup()

qt_add_executable(${PROJECT_NAME})

set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 20)

target_compile_options(${PROJECT_NAME} PUBLIC
  -Wall
  -Wextra
  -Wpedantic
  -pedantic-errors
  -O2
)

file(GLOB CPP_SOURCES CONFIGURE_DEPENDS "cpp/*.cpp")
file(GLOB UI_SOURCES CONFIGURE_DEPENDS "UI/*.ui")
file(GLOB HEADERS CONFIGURE_DEPENDS "include/*.h")

qt_add_resources(RESOURCES "images.qrc")

target_sources(${PROJECT_NAME} PRIVATE
  ${CPP_SOURCES}
  ${UI_SOURCES}
  ${HEADERS}
  ${RESOURCES}
)

target_include_directories(${PROJECT_NAME} PRIVATE include)

target_link_libraries(${PROJECT_NAME} PRIVATE
  Qt6::Widgets
  Qt6::Charts
  Qt6::DataVisualization
)

install(TARGETS ${PROJECT_NAME})
